/**********************************************
* File: ageHash2.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows a basic example of STL C++ Hash Map
**********************************************/
#include <string>
#include <iostream>
#include <unordered_map>

/********************************************
* Function Name  : main
* Pre-conditions :
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(){
	
	std::unordered_map<std::string, int> ageHash{ {"Matthew", 38}, {"Alfred", 72}, {"Grace", 14}, {"James", 35} };
	
	/* Get the age of the each */
	std::cout << "James' Age Is: " << ageHash["James"] << std::endl;
	std::cout << "Matthew's Age Is: " << ageHash["Matthew"] << std::endl;
	std::cout << "Alfred's Age Is: " << ageHash["Alfred"] << std::endl;
	std::cout << "Grace's Age Is: " << ageHash["Grace"] << std::endl;
	
	std::cout << std::endl << "This prints out a deliberately bad value:\n";
	std::cout << "BadValue's Age Is: " << ageHash["Bad Value"] << std::endl;
	
	/* Smart iterator prints out all values */
	
	std::cout << std::endl << "Print all the values in the Hash:\n";
	for( std::pair<const std::string, int>& thePair : ageHash ){
		
		std::cout << thePair.first << " " << thePair.second << std::endl;
		
	}
	
	return 0;
}

