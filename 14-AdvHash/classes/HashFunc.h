/**********************************************
* File: HashFunc.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains Hash Functions for different data types 
**********************************************/

#ifndef HASHFUNC_H
#define HASHFUNC_H

/********************************************
* Function Name  : HashFunc
* Pre-conditions : int Value 
* Post-conditions: int
* 
* Returns a Hash Value for integer 
********************************************/
int HashFunc( int Value ){
	
	return Value;
}


/********************************************
* Function Name  : HashFunc
* Pre-conditions : int Value 
* Post-conditions: int
* 
* Second Hash Function for Double Hashing 
********************************************/
int HashFunc2( int Value ){
	
	return 3 - (Value % 3);
	
}

/********************************************
* Function Name  : HashFunc
* Pre-conditions : std::string Value 
* Post-conditions: int
* 
* Returns a Hash Value for std::string 
********************************************/
int HashFunc( std::string Value ){

	return (int)Value.size();
}

/********************************************
* Function Name  : HashFunc
* Pre-conditions : std::string Value 
* Post-conditions: int
* 
* Second Hash Function for Double Hashing std::string
********************************************/
int HashFunc2( std::string Value ){

	return 3 - ( (int)Value.size() % 3 );

}

/********************************************
* Function Name  : HashFunc
* Pre-conditions : char Value 
* Post-conditions: int
* 
* Returns a Hash Value for char 
********************************************/
int HashFunc( char Value ){

	return (int)Value;
}

/********************************************
* Function Name  : HashFunc
* Pre-conditions : char Value 
* Post-conditions: int
* 
* Second Hash Function for Double Hashing char
********************************************/
int HashFunc2( char Value ){

	return 3 - ( (int)Value % 3 );

}

#endif

