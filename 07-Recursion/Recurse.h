/**********************************************
* File: Recurse.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains all the recusrion functions 
**********************************************/
#ifndef RECURSE_H
#define RECURSE_H

#include<cmath> // pow
#include <iostream>

/*
unsigned int factorial(unsigned int i){
	
	// Error Case 
	if(i < 0){
		std::cout << "Please enter a positive number" << std::endl;
		exit(-1);
	}
	
	// Base case i = 0
	if(i == 0){
		return 1;
	}

	return i* factorial(i-1);
}
*/

double factorial(unsigned int i){
	
	// Base case i = 0
	if(i == 0){
		return 1;
	}

	return i* factorial(i-1);
}

double exponential(double x, int k){
	
	if(k == 0)
		return 1;

	std::cout << "x   = " << x << ", ";
	std::cout << "x^n = " << pow(x,k) << ", ";
	std::cout << "n!  = " << factorial(k) << ", ";
	std::cout << "x^n/n! = " << (pow(x,k)/factorial(k)) << std::endl;

	return (pow(x,k)/factorial(k)) + exponential(x,k-1);
  
}


/********************************************
* Function Name  : sin
* Pre-conditions : double x, int n
* Post-conditions: double
* 
* Recursive function which calculates sin(x) 
********************************************/
double sin(double x, int n){

	if(n == 0)
		return x;
	
	return pow(-1, n)*pow(x, 2*n+1)/factorial(2*n+1) + sin(x, n-1);

}


void merge(int *array, int low, int high, int mid)
{
	// We have low to mid and mid+1 to high already sorted.
	int i, j, k;
	int *temp = new int[high-low+1];
	i = low;
	k = 0;
	j = mid + 1;
 
	// Merge the two parts into temp[].
	while (i <= mid && j <= high)
	{
		if (array[i] < array[j])
		{
			temp[k] = array[i];
			k++;
			i++;
		}
		else
		{
			temp[k] = array[j];
			k++;
			j++;
		}
	}
 
	// Insert all the remaining values from i to mid into temp[].
	while (i <= mid)
	{
		temp[k] = array[i];
		k++;
		i++;
	}
 
	// Insert all the remaining values from j to high into temp[].
	while (j <= high)
	{
		temp[k] = array[j];
		k++;
		j++;
	}
 
 
	// Assign sorted data stored in temp[] to a[].
	for (i = low; i <= high; i++)
	{
		array[i] = temp[i-low];
	}
	
	// Delete the temp array
	delete[] temp;
}


void mergeSort(int *array, int low, int high){
	
	if(low < high){
		
		int mid = (low + high) / 2;
		
		mergeSort(array, low, mid);
		mergeSort(array, mid+1, high);
		merge(array, low, high, mid);
		
	}
}


double Fib(double n){
	
	if(n == 0){
		return 1;
	}
	else if(n == 1){
		return 1;
	}
	
	return Fib(n-1) + Fib(n-2);
	
	return 0;
}

#endif
