
#ifndef FACT_H
#define FACT_H


#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf

/********************************************
* Function Name  : noBadChars
* Pre-conditions : std::string testString
* Post-conditions: bool
* 
* Returns false if any character other than 0-9
* '.', or '-' appears in the string
********************************************/
bool noBadChars(std::string testString);


/********************************************
* Function Name  : getValue
* Pre-conditions : std::string strUnsigX, unsigned int& unsigX
* Post-conditions: none
* 
* Overloaded C++ Function that tests inputs for unsigned int 
********************************************/
void getValue(std::string strUnsigX, unsigned int& unsigX);


/********************************************
* Function Name  : factorial
* Pre-conditions : unsigned int i
* Post-conditions: double
*  
* Calculates i!
********************************************/
double factorial(unsigned int i);


#endif

